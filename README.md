# TODO  

## Core functions

- List all pods  
  kubectl get pods

- Connect to given pod
  kubectl exec -it <pod> bash

- List all containers of given pod  
  kubectl get  

- Get log from container  
  kubectl logs <pod> -c <container>


## Additional 
- Switch to common directory

- Tail application.log

- Add aliases to session

